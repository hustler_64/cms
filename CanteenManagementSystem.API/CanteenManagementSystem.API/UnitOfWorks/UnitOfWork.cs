﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CanteenManagementSystem.API.Data;
using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;

namespace CanteenManagementSystem.API.UnitOfWorks
{
    #region Interface
    public interface IUnitOfWork
    {
        void Commit();

        IGenericRepository<AppUser> UserRepository { get; }
        IGenericRepository<MenuCategory> MenuCategory { get; }
        IGenericRepository<Menus> Menus { get; }
        IGenericRepository<FoodOrder> Orders { get; }

    }
    #endregion
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CanteenManagementDbContext _context;

        public UnitOfWork(CanteenManagementDbContext context)
        {
            _context = context;
        }

        public IGenericRepository<AppUser> _AppUser;
        public IGenericRepository<FoodOrder> _order;
        public IGenericRepository<Menus> _menu;
        public IGenericRepository<MenuCategory> _category;

        public IGenericRepository<AppUser> UserRepository
        {
            get { return _AppUser ?? (_AppUser = new GenericRepository<AppUser>(_context)); }
        }

        public IGenericRepository<MenuCategory> MenuCategory
        {
            get { return _category ?? (_category = new GenericRepository<MenuCategory>(_context)); }
        }

        public IGenericRepository<Menus> Menus
        {
            get { return _menu ?? (_menu = new GenericRepository<Menus>(_context)); }
        }

        public IGenericRepository<FoodOrder> Orders
        {
            get { return _order ?? (_order = new GenericRepository<FoodOrder>(_context)); }
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
