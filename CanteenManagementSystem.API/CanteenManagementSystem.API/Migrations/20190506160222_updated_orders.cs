﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CanteenManagementSystem.API.Migrations
{
    public partial class updated_orders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodOrders_AspNetUsers_UserId",
                table: "FoodOrders");

            migrationBuilder.DropIndex(
                name: "IX_FoodOrders_UserId",
                table: "FoodOrders");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "FoodOrders");

            migrationBuilder.AddColumn<string>(
                name: "CustomerName",
                table: "FoodOrders",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TableNumber",
                table: "FoodOrders",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerName",
                table: "FoodOrders");

            migrationBuilder.DropColumn(
                name: "TableNumber",
                table: "FoodOrders");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "FoodOrders",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_FoodOrders_UserId",
                table: "FoodOrders",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodOrders_AspNetUsers_UserId",
                table: "FoodOrders",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
