﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CanteenManagementSystem.API.Migrations
{
    public partial class foreignKey_Updated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MenuCategoryId",
                table: "Menus",
                newName: "MenuCatId");

            migrationBuilder.CreateIndex(
                name: "IX_Menus_MenuCatId",
                table: "Menus",
                column: "MenuCatId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodOrders_MenuId",
                table: "FoodOrders",
                column: "MenuId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodOrders_UserId",
                table: "FoodOrders",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodOrders_Menus_MenuId",
                table: "FoodOrders",
                column: "MenuId",
                principalTable: "Menus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodOrders_AspNetUsers_UserId",
                table: "FoodOrders",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_MenuCategories_MenuCatId",
                table: "Menus",
                column: "MenuCatId",
                principalTable: "MenuCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodOrders_Menus_MenuId",
                table: "FoodOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_FoodOrders_AspNetUsers_UserId",
                table: "FoodOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_MenuCategories_MenuCatId",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_Menus_MenuCatId",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_FoodOrders_MenuId",
                table: "FoodOrders");

            migrationBuilder.DropIndex(
                name: "IX_FoodOrders_UserId",
                table: "FoodOrders");

            migrationBuilder.RenameColumn(
                name: "MenuCatId",
                table: "Menus",
                newName: "MenuCategoryId");
        }
    }
}
