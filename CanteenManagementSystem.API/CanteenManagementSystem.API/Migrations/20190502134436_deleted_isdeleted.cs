﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CanteenManagementSystem.API.Migrations
{
    public partial class deleted_isdeleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Menus");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "MenuCategories");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "FoodOrders");

            migrationBuilder.AlterColumn<double>(
                name: "TotalPrice",
                table: "FoodOrders",
                nullable: true,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Menus",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "MenuCategories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<double>(
                name: "TotalPrice",
                table: "FoodOrders",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "FoodOrders",
                nullable: false,
                defaultValue: false);
        }
    }
}
