﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CanteenManagementSystem.API.Migrations
{
    public partial class updates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodOrders_AspNetUsers_CreatedById",
                table: "FoodOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_FoodOrders_AspNetUsers_UpdatedById",
                table: "FoodOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuCategories_AspNetUsers_CreatedById",
                table: "MenuCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuCategories_AspNetUsers_UpdatedById",
                table: "MenuCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_AspNetUsers_CreatedById",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_AspNetUsers_UpdatedById",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_Menus_CreatedById",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_Menus_UpdatedById",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_MenuCategories_CreatedById",
                table: "MenuCategories");

            migrationBuilder.DropIndex(
                name: "IX_MenuCategories_UpdatedById",
                table: "MenuCategories");

            migrationBuilder.DropIndex(
                name: "IX_FoodOrders_CreatedById",
                table: "FoodOrders");

            migrationBuilder.DropIndex(
                name: "IX_FoodOrders_UpdatedById",
                table: "FoodOrders");

            migrationBuilder.AlterColumn<bool>(
                name: "Deleted",
                table: "Menus",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "Deleted",
                table: "MenuCategories",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "Deleted",
                table: "FoodOrders",
                nullable: true,
                oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Deleted",
                table: "Menus",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Deleted",
                table: "MenuCategories",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Deleted",
                table: "FoodOrders",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Menus_CreatedById",
                table: "Menus",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Menus_UpdatedById",
                table: "Menus",
                column: "UpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MenuCategories_CreatedById",
                table: "MenuCategories",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_MenuCategories_UpdatedById",
                table: "MenuCategories",
                column: "UpdatedById");

            migrationBuilder.CreateIndex(
                name: "IX_FoodOrders_CreatedById",
                table: "FoodOrders",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_FoodOrders_UpdatedById",
                table: "FoodOrders",
                column: "UpdatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodOrders_AspNetUsers_CreatedById",
                table: "FoodOrders",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodOrders_AspNetUsers_UpdatedById",
                table: "FoodOrders",
                column: "UpdatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuCategories_AspNetUsers_CreatedById",
                table: "MenuCategories",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuCategories_AspNetUsers_UpdatedById",
                table: "MenuCategories",
                column: "UpdatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_AspNetUsers_CreatedById",
                table: "Menus",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_AspNetUsers_UpdatedById",
                table: "Menus",
                column: "UpdatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
