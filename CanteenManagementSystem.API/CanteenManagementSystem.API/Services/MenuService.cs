﻿using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.UnitOfWorks;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Services
{
    public class MenuService : IMenuService
    {
        private readonly CanteenManagementDbContext _context;
        private readonly IUnitOfWork _unitOfWork;

        public MenuService( CanteenManagementDbContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }
        public ValidationMessage DeleteMenu(Guid id)
        {
            try
            {
                var result = new ValidationMessage();
                var menu = _context.Menus.Find(id);
                if (menu == null)
                {
                    return result;
                }
                _context.Menus.Remove(menu);
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationMessage EditMenu(Menus menu)
        {
            try
            {
                var result = new ValidationMessage();
                //var menuDetails = _unitOfWork.Menus.FindByCondition(x => x.Id == menu.Id).FirstOrDefault();
                var menuDetails = _context.Menus.ToList().Where(x => x.Id == menu.Id).FirstOrDefault();
                if (menuDetails == null)
                {
                    return result;
                }
                menuDetails.ModifiedDate = DateTime.Now;
                menuDetails.Remarks = menu.Remarks;
                menuDetails.UpdatedById = menu.UpdatedById;
                menuDetails.Menu = menu.Menu;
                menuDetails.Price = menu.Price;
                menuDetails.MenuCatId = menu.MenuCatId;
                _context.Entry(menuDetails).State = EntityState.Modified;
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MenuViewModel> GetMenu()
        {
            try
            {
                var query =
                    from menuCat in _context.MenuCategories
                    join menu in _context.Menus on menuCat.Id equals menu.MenuCatId
                    orderby menu.CreatedDate descending
                    select new MenuViewModel()
                    {
                        Id = menu.Id,
                        CreatedById = menu.CreatedById,
                        CreatedDate = menu.CreatedDate,
                        Deleted = menu.Deleted,
                        Menu = menu.Menu,
                        MenuCatId = menu.MenuCatId,
                        MenuCatName = menu.MenuCatDetails.MenuCategoriesName,
                        MenuCatRemarks = menu.MenuCatDetails.Remarks,
                        Price = menu.Price,
                        Remarks = menu.Remarks,
                        ModifiedDate = menu.ModifiedDate,
                        UpdatedById = menu.UpdatedById
                    };
                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMenuCount()
        {
            try
            {
                var menuCount = _context.Menus.ToList().Count();
                return menuCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get menu according to the menu category id passed
        /// </summary>
        /// <returns></returns>
        public List<MenuViewModel> GetMenuByCid(Guid Id)
        {
            try
            {
                var data = (from b in _unitOfWork.Menus.GetAll()
                           where b.MenuCatId == Id
                           select new MenuViewModel
                           {
                               CreatedById = b.CreatedById,
                               CreatedDate = b.CreatedDate,
                               Deleted = b.Deleted,
                               Menu = b.Menu,
                               MenuCatName = b.MenuCatDetails.MenuCategoriesName,
                               MenuCatRemarks = b.MenuCatDetails.Remarks,
                               MenuCatId = b.MenuCatId,
                               Price = b.Price,
                               Remarks = b.Remarks,
                               ModifiedDate = b.ModifiedDate,
                               UpdatedById = b.UpdatedById
                           }).ToList();
                //var menus = _context.Menus.Where(x => x.MenuCatId == Id).ToList().Select(t =>
                //new MenuViewModel()
                //{
                //    CreatedById = t.CreatedById,
                //    CreatedDate = t.CreatedDate,
                //    Deleted = t.Deleted,
                //    Menu = t.Menu,
                //    MenuCatName = t.MenuCatDetails.MenuCategoriesName,
                //    MenuCatRemarks = t.MenuCatDetails.Remarks,
                //    MenuCatId = t.MenuCatId,
                //    Price = t.Price,
                //    Remarks = t.Remarks,
                //    ModifiedDate = t.ModifiedDate,
                //    UpdatedById = t.UpdatedById
                //}).FirstOrDefault();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationMessage SaveMenu(Menus menu)
        {

            try
            {
                var result = new ValidationMessage();
                var menus = new Menus
                {
                    Menu = menu.Menu,
                    Price = menu.Price,
                    MenuCatId = menu.MenuCatId,
                    CreatedById = menu.CreatedById,
                    Remarks = menu.Remarks,
                    CreatedDate = DateTime.Now,
                    Deleted = false,
                    ModifiedDate = DateTime.Now,
                    UpdatedById = null,
                };
                _context.Menus.Add(menus);
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
