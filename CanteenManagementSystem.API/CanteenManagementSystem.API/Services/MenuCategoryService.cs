﻿using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Services
{
    public class MenuCategoryService : IMenuCategoryService
    {
        private readonly CanteenManagementDbContext _context;

        public MenuCategoryService(CanteenManagementDbContext context)
        {
            _context = context;
        }

        public ValidationMessage DeleteMenuCategory(Guid id)
        {
            try
            {
                var result = new ValidationMessage();
                var menu = _context.MenuCategories.Find(id);
                if (menu == null)
                {
                    return result;
                }
                _context.MenuCategories.Remove(menu);
                 _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationMessage EditMenuCategory(MenuCategory menu)
        {
            try
            {
                var result = new ValidationMessage();
                var menuCategoryDetails = _context.MenuCategories.ToList().Where(x => x.Id == menu.Id).FirstOrDefault();
                menuCategoryDetails.ModifiedDate = DateTime.Now;
                menuCategoryDetails.Remarks = menu.Remarks;
                menuCategoryDetails.UpdatedById = menu.UpdatedById;
                menuCategoryDetails.MenuCategoriesName = menu.MenuCategoriesName;
                _context.Entry(menuCategoryDetails).State = EntityState.Modified;
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MenuCategory> GetMenuCategory()
        {
            try
            {
                var data = _context.MenuCategories.ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetMenuCategoryCount()
        {
            try
            {
                return _context.MenuCategories.ToList().Count();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationMessage SaveMenuCategory(MenuCategory menuCategory)
        {
            try
            {
                var result = new ValidationMessage();
                var menuCategories = new MenuCategory
                {
                    MenuCategoriesName = menuCategory.MenuCategoriesName,
                    CreatedById = menuCategory.CreatedById,
                    Remarks = menuCategory.Remarks,
                    CreatedDate = DateTime.Now,
                    Deleted = false,
                    ModifiedDate = DateTime.Now,
                    UpdatedById = null,
                };
                _context.MenuCategories.Add(menuCategories);
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
