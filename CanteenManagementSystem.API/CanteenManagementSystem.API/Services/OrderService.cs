﻿using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Services
{
    public class OrderService : IOrderService
    {
        private readonly CanteenManagementDbContext _context;

        public OrderService(CanteenManagementDbContext context)
        {
            _context = context;
        }

        public ValidationMessage DeleteOrder(Guid id)
        {
            try
            {
                var result = new ValidationMessage();
                FoodOrder order = _context.FoodOrders.Find(id);

                _context.FoodOrders.Remove(order);
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationMessage EditOrder(FoodOrder order)
        {
            try
            {
                var result = new ValidationMessage();
                var orderDetails = _context.FoodOrders.ToList().Where(x => x.Id == order.Id).FirstOrDefault();
                orderDetails.MenuId = order.MenuId;
                orderDetails.ModifiedDate = DateTime.Now;
                orderDetails.CustomerName = order.CustomerName;
                orderDetails.TableNumber = order.TableNumber;
                orderDetails.UpdatedById = order.UpdatedById;
                orderDetails.Remarks = order.Remarks;
                orderDetails.TotalPrice = order.TotalPrice;

                _context.Entry(orderDetails).State = EntityState.Modified;
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrderViewModel> GetOrder()
        {
            try
            {
                var query = from menu in _context.Menus
                            join order in _context.FoodOrders on menu.Id equals order.MenuId
                            select new OrderViewModel()
                            {
                                CreatedById = order.CreatedById,
                                CreatedDate = order.CreatedDate,
                                Deleted = order.Deleted,
                                Id = order.Id,
                                MenuId = order.MenuId,
                                MenuName = menu.Menu,
                                MenuRemarks = menu.Remarks,
                                MenuCatrogoryName = menu.MenuCatDetails.MenuCategoriesName,
                                MenuCatRemarks = menu.MenuCatDetails.Remarks,
                                ModifiedDate = order.ModifiedDate,
                                OrderRemarks = order.Remarks,
                                Price = menu.Price,
                                TotalPrice = order.TotalPrice,
                                CustomerName = order.CustomerName,
                                TableNumber = order.TableNumber
                            };
                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get per day total sales
        /// </summary>
        /// <returns></returns>
        public List<OrderViewModel> GetPerDaySales()
        {
            try
            {
                var query = from menu in _context.Menus
                            join order in _context.FoodOrders on menu.Id equals order.MenuId
                            where order.CreatedDate.Date == DateTime.Now.Date
                            select new OrderViewModel()
                            {
                                CreatedById = order.CreatedById,
                                CreatedDate = order.CreatedDate,
                                Deleted = order.Deleted,
                                Id = order.Id,
                                MenuId = order.MenuId,
                                MenuName = menu.Menu,
                                MenuRemarks = menu.Remarks,
                                MenuCatrogoryName = menu.MenuCatDetails.MenuCategoriesName,
                                MenuCatRemarks = menu.MenuCatDetails.Remarks,
                                ModifiedDate = order.ModifiedDate,
                                OrderRemarks = order.Remarks,
                                Price = menu.Price,
                                TotalPrice = order.TotalPrice,
                                CustomerName = order.CustomerName,
                                TableNumber = order.TableNumber
                            };
                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetOrderCount()
        {
            try
            {
                var orderCount = _context.FoodOrders.ToList().Count();
                return orderCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public OrderViewModel GetOrdersByUid(Guid Id)
        //{
        //    try
        //    {
        //        var orders = _context.FoodOrders.Where(x => x.UserId == Id).ToList().Select(
        //        t => new OrderViewModel()
        //        {
        //            CreatedById = t.CreatedById,
        //            CreatedDate = t.CreatedDate,
        //            Deleted = t.Deleted,
        //            Id = t.Id,
        //            MenuId = t.MenuId,
        //            MenuName = t.MenuDetails.Menu,
        //            MenuRemarks = t.Remarks,
        //            MenuCatrogoryName = t.MenuDetails.MenuCatDetails.MenuCategoriesName,
        //            MenuCatRemarks = t.MenuDetails.MenuCatDetails.Remarks,
        //            ModifiedDate = t.ModifiedDate,
        //            OrderRemarks = t.Remarks,
        //            Price = t.MenuDetails.Price,
        //            TotalPrice = t.TotalPrice,
        //            UserId = t.UserId,
        //            UserName = t.UserDetails.UserName
        //        });
        //      return orders.FirstOrDefault();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public ValidationMessage SaveOrder(FoodOrder order)
        {
            try
            {
                var result = new ValidationMessage();
                var orders = new FoodOrder
                {
                    MenuId = order.MenuId,
                    CustomerName = order.CustomerName,
                    TableNumber = order.TableNumber,
                    Remarks = order.Remarks,
                    TotalPrice = order.TotalPrice,
                    CreatedDate = DateTime.Now,
                    CreatedById = order.CreatedById
                };
                _context.FoodOrders.Add(orders);
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationMessage SaveContactUs(ContactUs contactUs)
        {
            try
            {
                var result = new ValidationMessage();
                var orders = new ContactUs
                {
                    Email = contactUs.Email,
                    Message = contactUs.Message,
                    Name = contactUs.Name,
                    Subject = contactUs.Subject,
                    CreatedDate = DateTime.Now,
                };
                _context.ContactUs.Add(contactUs);
                _context.SaveChangesAsync();
                result.Result = true;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
