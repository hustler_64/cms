﻿using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Interfaces
{
    public interface IAccount
    {
        Task<IdentityResult> AddUser(AppUser User, string confirmationLink);

        Task<object> DoLogin(string Username, string Password, bool isWebLogin);

        Task<ValidationMessage> ForgotPassword(string userEmail);

        Task<ValidationMessage> ConfirmReset(string token, string userId, string newPassword);

        void SendConfirmationEmail(AppUser newUser, string confirmationLink);

        void SendConfirmationEmailOnNewEmail(AppUser newUser, string confirmationLink);

        bool IsValidEmailAddress(string email);
    }
}
