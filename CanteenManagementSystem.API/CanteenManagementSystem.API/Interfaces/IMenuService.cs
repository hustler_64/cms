﻿using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Interfaces
{
    public interface IMenuService
    {
        //get total menu name
        List<MenuViewModel> GetMenu();

        //get menu according to the menu category id
        List<MenuViewModel> GetMenuByCid(Guid Id);

        //get total menu count
        int GetMenuCount();

        //save menu to db
        ValidationMessage SaveMenu(Menus menu);

        //edit menu details
        ValidationMessage EditMenu(Menus menu);

        //delete menu
        ValidationMessage DeleteMenu(Guid id);
    }
}
