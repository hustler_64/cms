﻿using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Interfaces
{
    public interface IOrderService
    {
        //get all order name
        List<OrderViewModel> GetOrder();

        //get per day total sales
        List<OrderViewModel> GetPerDaySales();

        //get total order count
        int GetOrderCount();

        //get orders by faculty id
        //OrderViewModel GetOrdersByUid(Guid Id);

        //save orders to db
        ValidationMessage SaveOrder(FoodOrder order);

        //save contact us
        ValidationMessage SaveContactUs(ContactUs contactUs);

        //edit orders details
        ValidationMessage EditOrder(FoodOrder order);

        //delete orders
        ValidationMessage DeleteOrder(Guid id);
    }
}
