﻿using CanteenManagementSystem.API.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Interfaces
{
   public interface IMailSender
    {
        void SendMailOnNewUser(IOptions<MailSettings> mailSettings, string userName, string email, string confirmationLink);
        void SendMailOnResetPassword(IOptions<MailSettings> mailSettings, string userName, string code, string userEmail, Guid userId);
        void SendMail(List<string> to, string subject, string body, IOptions<MailSettings> mailSettings, List<string> ccMailAddress = null);

    }
}
