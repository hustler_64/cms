﻿using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Interfaces
{
    public interface IMenuCategoryService
    {
        //get total MenuCategory name
        List<MenuCategory> GetMenuCategory();

        //get total MenuCategory count
        int GetMenuCategoryCount();

        //save MenuCategory to db
        ValidationMessage SaveMenuCategory(MenuCategory menuCategory);

        //edit MenuCategory details
        ValidationMessage EditMenuCategory(MenuCategory menu);

        //delete MenuCategory
        ValidationMessage DeleteMenuCategory(Guid id);
    }
}
