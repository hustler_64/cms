﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CanteenManagementSystem.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("CanteenManagementPolicy")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private IMenuService _menuService;

        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }
        #region Menu

        /// <summary>
        /// Get all the menu name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("totalMenu")]
        public IActionResult GetMenus()
        {
            var menu = _menuService.GetMenu();
            return Ok(menu);
        }

        /// <summary>
        /// get mnenus according to the menu category id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getMenuByCId/{id}")]
        public IActionResult GetMenuByCid(Guid Id)
        {
            var result = _menuService.GetMenuByCid(Id);
            return Ok(result);
        }

        /// <summary>
        /// Get total count of all menu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("menuCount")]
        public int GetTotalMenuCount()
        {
            var count = _menuService.GetMenuCount();
            return count;
        }

        /// <summary>
        /// save the menu in db
        /// </summary>
        [HttpPost]
        [Route("saveMenus")]
        public IActionResult SaveMenu([FromBody]Menus menu)
        {
            try
            {
               var result = _menuService.SaveMenu(menu);
                return Ok(result);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// edit the menu details
        /// </summary>
        [HttpPut]
        [Route("editMenu")]
        public IActionResult EditMenu([FromBody]Menus menu)
        {
            var result =_menuService.EditMenu(menu);
            return Ok(result);
        }

        /// <summary>
        /// delete menu from db
        /// </summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{id}")]
        [Route("deleteMenu/{id}")]
        public IActionResult DeleteMenu(Guid id)
        {
            var result = _menuService.DeleteMenu(id);
            return Ok(result);
        }

        #endregion Menu
    }
}