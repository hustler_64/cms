﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CanteenManagementSystem.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("CanteenManagementPolicy")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Get all the Orders name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("totalOrder")]
        public IActionResult GetOrder()
        {
            var prog = _orderService.GetOrder();
            return Ok(prog);
        } 
        
        /// Get all the Orders name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("perDaySales")]
        public IActionResult GetPerDaySales()
        {
            var result = _orderService.GetPerDaySales();
            return Ok(result);
        }

        /// <summary>
        /// Get total count of all Orders
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("OrderCount")]
        public int GetTotalOrderCount()
        {
            var count = _orderService.GetOrderCount();
            return count;
        }

        /// <summary>
        /// get Orders according to the university id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        //[HttpGet]
        //[Route("getOrderByUId/id")]
        //public IActionResult GetOrdersByUid(Guid Id)
        //{
        //    var orderById = _orderService.GetOrdersByUid(Id);
        //    return Ok(orderById);
        //}

        /// <summary>
        /// save the Order in db
        /// </summary>
        [HttpPost]
        [Route("saveOrders")]
        public IActionResult SaveOrder([FromBody]FoodOrder Orders)
        {
            var result = _orderService.SaveOrder(Orders);
            return Ok(result);
        }

        /// <summary>
        /// edit the Order details
        /// </summary>
        [HttpPut]
        [Route("editOrder")]
        public IActionResult EditOrder([FromBody]FoodOrder Order)
        {
            var result = _orderService.EditOrder(Order);
            return Ok(result);
        }

        /// <summary>
        /// delete Order from db
        /// </summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{id}")]
        [Route("deleteOrder/id")]
        public IActionResult DeleteOrder(Guid id)
        {
            var result = _orderService.DeleteOrder(id);
            return Ok(id);
        }
    }
}