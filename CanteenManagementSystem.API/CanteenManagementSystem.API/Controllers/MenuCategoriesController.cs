﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CanteenManagementSystem.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("CanteenManagementPolicy")]
    [ApiController]
    public class MenuCategoriesController : ControllerBase
    {
        private IMenuCategoryService _service;
        public MenuCategoriesController(IMenuCategoryService service)
        {
            _service = service;
        }
        #region MenuCategories

        /// <summary>
        /// Get all the MenuCategories name
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("totalMenuCategories")]
        public IActionResult GetMenuCategories()
        {
            var menu = _service.GetMenuCategory();
            return Ok(menu);
        }

        /// <summary>
        /// Get total count of all MenuCategories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("menuCategoriesCount")]
        public int GetTotalMenuCategoriesCount()
        {
            var count = _service.GetMenuCategoryCount();
            return count;
        }

        /// <summary>
        /// save the MenuCategories in db
        /// </summary>
        [HttpPost]
        [Route("saveMenuCategories")]
        public IActionResult SaveMenuCategories([FromBody]MenuCategory menuCategories)
        {
            
            var result =_service.SaveMenuCategory(menuCategories);
            return Ok(result);
            
        }

        /// <summary>
        /// edit the MenuCategories details
        /// </summary>
        [HttpPut]
        [Route("editMenuCategories")]
        public IActionResult EditMenuCategories([FromBody]MenuCategory menuCategories)
        {
            var result = _service.EditMenuCategory(menuCategories);
            return Ok(result);
        }

        /// <summary>
        /// delete MenuCategories from db
        /// </summary>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpDelete("{id}")]
        [Route("deleteMenuCategory/{id}")]
        public IActionResult DeleteMenuCategory(Guid id)
        {
            var result = _service.DeleteMenuCategory(id);
            return Ok(result);
        }

        #endregion MenuCategories
    }
}