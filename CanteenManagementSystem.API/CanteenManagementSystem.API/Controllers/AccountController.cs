﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using CanteenManagementSystem.API.DataTransferObject;
using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using CanteenManagementSystem.API.Models;
using CanteenManagementSystem.API.Services;
using CanteenManagementSystem.API.UnitOfWorks;
using CanteenManagementSystem.API.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace CanteenManagementSystem.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [EnableCors("CanteenManagementPolicy")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IAccount _account;
        private readonly CanteenManagementDbContext _dbContext;
        private readonly IJwtService _jwtService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IUserService<AppUser> _userService;
        private readonly UserManager<AppUser> _userManager;
        private readonly IOrderService _orderService;

        public AccountController(UserManager<AppUser> userManager, IOrderService orderService, IUnitOfWork unitOfWork, IAccount account, IMapper mapper, CanteenManagementDbContext dbContext, IJwtService jwtService, IOptions<JwtIssuerOptions> jwtOptions, IUserService<AppUser> userService)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _jwtService = jwtService;
            _jwtOptions = jwtOptions.Value;
            _userService = userService;
            _account = account;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _orderService = orderService;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginDTO credentials)
        {
            try
            {
                var result = await _account.DoLogin(credentials.Username, credentials.Password, false);
                List<dynamic> loginToken = new List<dynamic>();
                foreach (var item in result.GetType().GetProperties())
                {
                    loginToken.Add(item.GetValue(result, null));
                }
                if (loginToken.Count == 0 && !(bool)result)
                    return BadRequest("Login failed. Wrong username or password.");

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest("Login failed. " + ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Register([FromBody]RegisterDTO newUser)
        {
            try
            {
                AppUser appUser = new AppUser();
                appUser.Id = Guid.NewGuid();
                appUser.UserName = newUser.Username;
                appUser.Email = newUser.Email;
                appUser.Password = newUser.Password;

                bool isValidEmail = _account.IsValidEmailAddress(newUser.Email);

                if (isValidEmail == false)
                {
                    return BadRequest("Please Enter valid Email");
                }

                if (newUser.ConfirmPassword != newUser.Password)
                {
                    return BadRequest("Password and confirm password mismatch");
                }

                string confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(appUser);
                string confirmationLink = Url.Action("ConfirmEmail",
                  "Account", new
                  {
                      userid = appUser.Id,
                      token = confirmationToken
                  }, protocol: HttpContext.Request.Scheme);
                var result = await _account.AddUser(appUser, confirmationLink);
                await SendReconfirmationEmail(appUser.UserName);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest("Registration failed. " + ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody] ResetPasswordViewModel email)
        {
            if (email != null)
            {
                var result = await _account.ForgotPassword(email.Email);
                return Ok(result);
            }
            return NotFound("email required.");
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmReset([FromBody] ForgotPasswordViewModel passwordReset)
        {
            var data = new ValidationMessage();
            if (passwordReset.Token != null && passwordReset.UserId != null && passwordReset.Password != null)
            {
                var result = await _account.ConfirmReset(passwordReset.Token, passwordReset.UserId, passwordReset.Password);
                data.Result = result.Result;
                data.Message = result.Message;
                return Ok(data);
            }
            return Ok(data);
        }

        /// <summary>
        /// Set ConfirmEmail true if email verified
        /// </summary>
        /// <param name="Users Id"></param>
        /// <param name="Generate EmailConfirmation Token"></param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult ConfirmEmail(string userid, string token)
        {
            AppUser user = _userManager.FindByIdAsync(userid).Result;
            IdentityResult result =  _userManager.ConfirmEmailAsync(user, token).Result;
            var data = new EmailConfirmationDTO();
            data.Username = user.UserName;
            data.Success = result.Succeeded;
            return View(data);
        }

        /// <summary>
        /// Send Confirm link to Email...
        /// </summary>
        /// <param name="username of user"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendReconfirmationEmail(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            string confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            string confirmationLink = Url.Action("ConfirmEmail",
             "Account", new
             {
                 userid = user.Id,
                 token = confirmationToken
             }, protocol: HttpContext.Request.Scheme);
            _account.SendConfirmationEmail(user, confirmationLink);
        }

        /// <summary>
        /// User Email and Username Updates
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> UpdateUser([FromBody] UpdateUser updateUser)
        {
            var result = new ValidationMessage();
            bool isValidEmail = _account.IsValidEmailAddress(updateUser.Email);
            string userId = _jwtService.GetCurrentUserId().ToString();
            AppUser user = await _userManager.FindByIdAsync(userId);
            try
            {
                if (isValidEmail != false)
                {
                    if (user.Email != updateUser.Email)
                    {
                        user.Email = updateUser.Email;
                        user.UserName = updateUser.Username;
                        user.EmailConfirmed = false;
                        await _userManager.UpdateAsync(user);
                        await SendConfirmationEmail(user.UserName);
                        result.Message = "Email Updated Successfully!";
                        result.Result = true;
                    }
                    else if (user.Email == updateUser.Email && user.UserName != updateUser.Username)
                    {
                        //user.Email = updateUser.Email;
                        user.UserName = updateUser.Username;
                        await _userManager.UpdateAsync(user);
                        result.Message = "Username Updated Successfully!";
                        result.Result = true;
                    }
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Result = false;
                return Ok(result);
            }
        }

        /// <summary>
        /// Send Confirm link to New Updated Email...
        /// </summary>
        /// <param name="username of user"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task SendConfirmationEmail(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            string confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            string confirmationLink = Url.Action("ConfirmEmail",
             "Account", new
             {
                 userid = user.Id,
                 token = confirmationToken
             }, protocol: HttpContext.Request.Scheme);
            _account.SendConfirmationEmailOnNewEmail(user, confirmationLink);
        }

        /// <summary>
        /// Gets User Email and Username
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetUserDetail()
        {
            try
            {
                var query =  _unitOfWork.UserRepository.GetAll().ToList();
                return Ok(query);
            }
            catch (Exception ex)
            {
                return BadRequest("error" + ex.Message);
            }
        }

        /// <summary>
        /// User Password Updates
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> ChangePassword([FromBody] UpdateUser updateUser)
        {
            var result = new ValidationMessage();
            string userId = _jwtService.GetCurrentUserId().ToString();
            AppUser user = await _userManager.FindByIdAsync(userId);
            try
            {
                bool checkPassword = await _userManager.CheckPasswordAsync(user, updateUser.oldPassword);
                if (checkPassword == true)
                {
                    await _userManager.ChangePasswordAsync(user, updateUser.oldPassword, updateUser.newPassword);
                    result.Result = true;
                    result.Message = "Password Changed Successfully!";
                    return Ok(result);
                }
                else
                {
                    result.Result = false;
                    result.Message = "Old Password does not match";
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Result = false;
                result.Message = ex.Message;
                return Ok(result);
            }
        }

        /// <summary>
        /// add contact us
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SaveMessage([FromBody]ContactUs contactUs)
        {
            try
            {
                var result = _orderService.SaveContactUs(contactUs);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest("error" + ex.Message);
            }
        }

    }
}