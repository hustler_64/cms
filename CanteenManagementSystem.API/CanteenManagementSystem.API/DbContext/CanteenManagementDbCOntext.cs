﻿using CanteenManagementSystem.API.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.DbContext
{   
    public class CanteenManagementDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public CanteenManagementDbContext(DbContextOptions<CanteenManagementDbContext> options)
           : base(options)
        {
        }

        public DbSet<Menus> Menus { get; set; }
        public DbSet<MenuCategory> MenuCategories { get; set; }
        public DbSet<FoodOrder> FoodOrders { get; set; }
        public DbSet<ContactUs> ContactUs { get; set; }
    }
}
