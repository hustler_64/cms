﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.DataTransferObject
{
    public class EmailConfirmationDTO
    {
        public string Username { get; set; }
        public bool Success { get; set; }
        public string Error { get; set; }
    }
}
