﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Models
{
    public class Menus : LogEntity
    {
        [StringLength(500)]
        public string Menu { get; set; }

        public double Price { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }


        public Guid MenuCatId { get; set; }
        [ForeignKey("MenuCatId")]
        public virtual MenuCategory MenuCatDetails { get; set; }
    }
}
