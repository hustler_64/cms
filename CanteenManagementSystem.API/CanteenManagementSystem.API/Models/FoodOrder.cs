﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Models
{
    public class FoodOrder : LogEntity
    {
        public Guid MenuId { get; set; }
        [ForeignKey("MenuId")]
        public virtual Menus MenuDetails { get; set; }

        [StringLength(1000)]
        public string CustomerName { get; set; }

        public int TableNumber { get; set; }

        [StringLength(1000)]
        public string Remarks { get; set; }

        public double? TotalPrice { get; set; }

    }
}
