﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Models
{
    public class MenuCategory : LogEntity
    {
        [StringLength(500)]
        public string MenuCategoriesName { get; set; }

        [StringLength(500)]
        public string Remarks { get; set; }
    }
}
