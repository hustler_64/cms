﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Models
{
    public abstract class LogEntity : DataEntity
    {
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime? ModifiedDate { get; set; }

        [ForeignKey("CreatedBy")]
        public Guid CreatedById { get; set; }

        [ForeignKey("UpdatedBy")]
        public Guid? UpdatedById { get; set; }
        
        public bool? Deleted { get; set; }
    }
}
