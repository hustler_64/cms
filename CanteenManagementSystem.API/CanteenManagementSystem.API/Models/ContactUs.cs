﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Models
{
    public class ContactUs : LogEntity
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
        public string Email { get; set; }
    }
}
