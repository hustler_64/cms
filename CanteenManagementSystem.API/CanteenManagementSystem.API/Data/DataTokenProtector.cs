﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace CanteenManagementSystem.API.Data
{
    /// <summary>
    /// This class is for setting 
    /// expiration period to custom Timespan.
    /// </summary>
    /// <typeparam name="TUser"></typeparam>
    public class DataTokenProtector<TUser> : DataProtectorTokenProvider<TUser> where TUser : class
    {
        public DataTokenProtector(IDataProtectionProvider dataProtectionProvider, IOptions<DataProtectionTokenProviderOptions> options) : base(dataProtectionProvider, options)
        {
        }
    }

    public class DataTokenProtectorOptions : DataProtectionTokenProviderOptions { }
}