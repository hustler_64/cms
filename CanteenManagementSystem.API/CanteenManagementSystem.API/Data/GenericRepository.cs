﻿using CanteenManagementSystem.API.DbContext;
using CanteenManagementSystem.API.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.Data
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly CanteenManagementDbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(CanteenManagementDbContext context)
        {
            _context = context ?? throw new ArgumentNullException("Context was not supplied.");
            _dbSet = _context.Set<TEntity>();
        }

        public TEntity AddEntity(TEntity entityToAdd)
        {
            _dbSet.Add(entityToAdd);
            //Save();
            return entityToAdd;
        }

        public void DeleteEntity(TEntity entityToDelete)
        {
            _dbSet.Attach(entityToDelete);
            _dbSet.Remove(entityToDelete);
            //Save();
        }

        public TEntity UpdateEntity(TEntity entityToUpdate)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
            //Save();
            return entityToUpdate;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet.AsNoTracking();
        }

        public IEnumerable<TEntity> GetAllData()
        {
            return _dbSet.AsEnumerable();
        }

        public IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate);
        }

        public IQueryable<TEntity> GetAll(
             Expression<Func<TEntity, bool>> filter = null,
             Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        public TEntity GetEntityByStringId(Guid guid) => _dbSet.Find(guid.ToString());

        public TEntity GetEntityById(Guid guid) => _dbSet.Find(guid);

        public void AddEntityList(IEnumerable<TEntity> listOfEntity)
        {
            _dbSet.AddRange(listOfEntity);
            //Save();
        }

        public TEntity AddEntityWithNoSave(TEntity entityToAdd)
        {
            _dbSet.Add(entityToAdd);
            return entityToAdd;
        }

        public void AddEntityListWithNoSave(List<TEntity> entities)
        {
            _dbSet.AddRange(entities);
        }

        public void UpdateEntityListWithNoSave(List<TEntity> entities)
        {
            _dbSet.UpdateRange(entities);
        }

        public void DeleteEntityListWithNoSave(List<TEntity> entities)
        {
            _dbSet.AttachRange(entities);
            _dbSet.RemoveRange(entities);
        }

        #region AsyncMethods

        public async Task<TEntity> AddEntityAsync(TEntity entityToAdd)
        {
            await _dbSet.AddAsync(entityToAdd);
            //await SaveAsync();
            return entityToAdd;
        }

        public async Task<int> DeleteEntityAsync(TEntity entityToDelete)
        {
            _dbSet.Remove(entityToDelete);
            return await SaveAsync();
        }

        public async Task<IEnumerable<TEntity>> FindByConditionAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<TEntity> GetEntityByIdAsync(Guid guid)
        {
            return await _dbSet.FindAsync(guid);
        }

        #endregion AsyncMethods

        public void Save() => _context.SaveChanges();

        public async Task<int> SaveAsync() => await _context.SaveChangesAsync();

        public TEntity UpdateEntityWithNoSave(TEntity entityToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}