﻿using CanteenManagementSystem.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.ViewModels
{
    public class MenuCategoryViewModel : LogEntity
    {
        public string MenuCategoriesName { get; set; }
        public string Remarks { get; set; }
    }
}
