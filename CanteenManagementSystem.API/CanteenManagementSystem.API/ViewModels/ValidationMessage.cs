﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.ViewModels
{
    public class ValidationMessage
    {
        public bool Result { get; set; }
        public string Message { get; set; }
    }
}
