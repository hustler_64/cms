﻿using CanteenManagementSystem.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.ViewModels
{
    public class MenuViewModel : LogEntity
    {
        public string Menu { get; set; }
        public double Price { get; set; }
        public string Remarks { get; set; }
        public string MenuCatName{ get; set; }
        public string MenuCatRemarks { get; set; }
        public Guid MenuCatId { get; set; }
    }
}
