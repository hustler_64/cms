﻿using CanteenManagementSystem.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.ViewModels
{
    public class OrderViewModel : LogEntity
    {
        public Guid MenuId { get; set; }
        public string CustomerName { get; set; }
        public int TableNumber { get; set; }
        public string OrderRemarks { get; set; }
        public double? TotalPrice { get; set; }
        public string MenuName { get; set; }
        public string MenuCatrogoryName { get; set; }
        public double Price { get; set; }
        public string MenuRemarks { get; set; }
        public string MenuCatRemarks { get; set; }
        public string UserName { get; set; }

    }
}
