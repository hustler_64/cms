﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanteenManagementSystem.API.ViewModels
{
    public class ContactUsViewModel
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
        public string Email { get; set; }
    }
}
