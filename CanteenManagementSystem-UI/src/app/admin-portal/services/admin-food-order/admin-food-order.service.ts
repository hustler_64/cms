import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ITotal } from '../../models/count-model';
import { IOrders } from '../../models/food-order-model';

@Injectable({
  providedIn: 'root'
})
export class AdminFoodOrderService {

  constructor(private _http: HttpClient) { }

  // http interceptor
  httpOptions() {
    var token = localStorage.getItem('auth_token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    return httpOptions;
  }

  //get all Menu name
  getOrderDetails(): Observable<IOrders[]> {
    return this._http.get<IOrders[]>(`${environment.apiUrl}Orders/totalOrder`, this.httpOptions());
  }

  getOrderCount(): Observable<ITotal>{
    return this._http.get<ITotal>(`${environment.apiUrl}Orders/OrderCount`, this.httpOptions());
  }

  //get all Menu name
  getPerDaySales(): Observable<IOrders[]> {
    return this._http.get<IOrders[]>(`${environment.apiUrl}Orders/perDaySales`, this.httpOptions());
  }

}
