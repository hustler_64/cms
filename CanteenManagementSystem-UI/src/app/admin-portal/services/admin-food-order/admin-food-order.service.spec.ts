import { TestBed, inject } from '@angular/core/testing';

import { AdminFoodOrderService } from './admin-food-order.service';

describe('AdminFoodOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminFoodOrderService]
    });
  });

  it('should be created', inject([AdminFoodOrderService], (service: AdminFoodOrderService) => {
    expect(service).toBeTruthy();
  }));
});
