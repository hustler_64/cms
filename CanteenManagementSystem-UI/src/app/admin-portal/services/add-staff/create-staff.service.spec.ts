import { TestBed, inject } from '@angular/core/testing';

import { CreateStaffService } from './create-staff.service';

describe('CreateStaffService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateStaffService]
    });
  });

  it('should be created', inject([CreateStaffService], (service: CreateStaffService) => {
    expect(service).toBeTruthy();
  }));
});
