import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IOrders } from '../../models/food-order-model';
import { environment } from 'src/environments/environment';
import { IUserRegister } from '../../models/user-model';

@Injectable({
  providedIn: 'root'
})
export class CreateStaffService {

  constructor(private _http: HttpClient) { }
  baseUrl = environment.apiUrl;

  // http interceptor
  httpOptions() {
    var token = localStorage.getItem('auth_token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    return httpOptions;
  }

  protected getCommonOptions() {
    const httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };
    return httpOptions;
}

   //get all Menu name
   getUserDetails(): Observable<IUserRegister[]> {
    return this._http.get<IUserRegister[]>(`${environment.apiUrl}Account/GetUserDetail`, this.httpOptions());
  }

  registerUser(entity):  Observable<any> {
    const url = this.baseUrl + 'Account/Register';
    return this._http.post<any>(url, entity, this.getCommonOptions());
  }
}
