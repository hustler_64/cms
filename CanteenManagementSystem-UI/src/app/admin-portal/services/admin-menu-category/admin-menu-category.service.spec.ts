import { TestBed, inject } from '@angular/core/testing';

import { AdminMenuCategoryService } from './admin-menu-category.service';

describe('AdminMenuCategoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminMenuCategoryService]
    });
  });

  it('should be created', inject([AdminMenuCategoryService], (service: AdminMenuCategoryService) => {
    expect(service).toBeTruthy();
  }));
});
