import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMenus } from '../../models/menu-model';
import { environment } from 'src/environments/environment';
import { ITotal } from '../../models/count-model';
import { IMenuCategories, MenuCategoryModel } from '../../models/menu-category-model';

@Injectable({
  providedIn: 'root'
})
export class AdminMenuCategoryService {

  constructor(private _http: HttpClient) { }

  // http interceptor
  httpOptions() {
    var token = localStorage.getItem('auth_token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    return httpOptions;
  }

  //get all Menu name
  getMenuCategoryName(): Observable<IMenuCategories[]> {
    return this._http.get<IMenuCategories[]>(`${environment.apiUrl}MenuCategories/totalMenuCategories`, this.httpOptions());
  }

  getMenuCategoryCount(): Observable<ITotal>{
    return this._http.get<ITotal>(`${environment.apiUrl}MenuCategories/menuCategoriesCount`, this.httpOptions());
  }

   //save Faculty
   saveMenuCategories(menuCategory: any) {
        var userId = localStorage.getItem('userId');
     const catDetails: any = {
      MenuCategoriesName: menuCategory.MenuCategoryName,
      Remarks: menuCategory.Remarks,
      CreatedById: userId
     }
    return this._http.post(`${environment.apiUrl}MenuCategories/saveMenuCategories`, catDetails, this.httpOptions());
  }

  //update Category
  updateCategory(menuCategory: any){
    var userId = localStorage.getItem('userId');
    const catDetails: any = {
     MenuCategoriesName: menuCategory.MenuCategoryName,
     Remarks: menuCategory.Remarks,
     UpdatedById: userId,
     Id: menuCategory.id
    }
    return this._http.put(`${environment.apiUrl}MenuCategories/editMenuCategories`, catDetails, this.httpOptions());

  }


  deleteCategory(id: string){
    return this._http.delete(`${environment.apiUrl}MenuCategories/deleteMenuCategory` + '/' + id, this.httpOptions());

  }

}
