import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMenus } from '../../models/menu-model';
import { environment } from 'src/environments/environment';
import { ITotal } from '../../models/count-model';

@Injectable({
  providedIn: 'root'
})
export class AdminMenuService {

  constructor(private _http: HttpClient) { }

  // http interceptor
  httpOptions() {
    var token = localStorage.getItem('auth_token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    return httpOptions;
  }

  //get all Menu name
  getMenuName(): Observable<IMenus[]> {
    return this._http.get<IMenus[]>(`${environment.apiUrl}Menu/totalMenu`, this.httpOptions());
  }

  //get all Menu name
  getMenuDetailsByCategoryId(id: string): Observable<IMenus[]> {
    return this._http.get<IMenus[]>(`${environment.apiUrl}Menu/getMenuByCId` + '/' + id, this.httpOptions());
  }

  getMenuCount(): Observable<ITotal>{
    return this._http.get<ITotal>(`${environment.apiUrl}Menu/menuCount`, this.httpOptions());
  }


   //save menus
  saveMenu(menus: any) {
    var userId = localStorage.getItem('userId');
    const menuDetails: any = {
      MenuCatId: menus.CatId,
      Remarks: menus.Remarks,
      CreatedById: userId,
      Menu: menus.MenuName,
      Price: menus.Price
    }
    return this._http.post(`${environment.apiUrl}Menu/saveMenus`, menuDetails, this.httpOptions());
  }

  //update menus
  updateMenu(menus: any) {
    var userId = localStorage.getItem('userId');
    const menuDetails: any = {
      MenuCatId: menus.CatId,
      Remarks: menus.Remarks,
      UpdatedById: userId,
      Menu: menus.MenuName,
      Price: menus.Price,
      Id: menus.id
    }
    return this._http.put(`${environment.apiUrl}Menu/editMenu`, menuDetails, this.httpOptions());

  }

  deleteMenu(id: string){
    return this._http.delete(`${environment.apiUrl}Menu/deleteMenu` + '/' + id, this.httpOptions());

  }

}
