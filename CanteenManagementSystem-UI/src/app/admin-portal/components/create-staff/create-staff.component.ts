import { Component, OnInit, ViewChild } from '@angular/core';
import { UserModel } from '../../models/user-model';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { AdminLoginService } from '../../services/admin-login/admin-login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CreateStaffService } from '../../services/add-staff/create-staff.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-create-staff',
  templateUrl: './create-staff.component.html',
  styleUrls: ['./create-staff.component.scss']
})
export class CreateStaffComponent implements OnInit {

  errormessage = "";
  user: UserModel;
  returnUrl = "";
  myData;
  registerForm: FormGroup;
  public userList: any;

  constructor(
    private registerService: CreateStaffService,
    public router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {}



  ngOnInit() {
    // login form group
    this.registerForm = this.formBuilder.group({
      userName:  new FormControl(null, Validators.required),
      email: ['', [Validators.required, Validators.email]],
      password: new FormControl(null, [Validators.required, Validators.minLength(7)]),
      confirmPassword: new FormControl(null, [Validators.required, Validators.minLength(7)])
    });

    this.getUsers();
  }

  /////////////////// for tables/////////////////
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['id', 'userName', 'email', 'emailConfirmed' ];

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  ///////////////////

     //reset form after submit
     resetForm(form?: NgForm) {
      if (form != null)
        form.reset();
      this.registerForm = this.formBuilder.group({
        userName: [''],
        email: [''],
        password: [''],
        confirmPassword: ['']
      });
    }


   // get user list
   getUsers() {
    this.registerService
      .getUserDetails()
      .subscribe((data: any) => {
        this.userList = data;
        console.log(this.userList);
        //to populate on table
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      });

  }

  OnSubmit(userDetails){
    this.registerService.registerUser(userDetails)
    .subscribe(
      (data: any) => {
        if (data.id == null) {
          this.resetForm();
          this.getUsers();
          this.toastr.success('New Staff is registered!', 'Staff');
        } else {
          this.toastr.error('Unable to add new staff!', 'Staff');
        }
      });
  }
}
