import { Component, OnInit } from '@angular/core';
import { AdminMenuService } from '../../services/admin-menu/admin-menu.service';
import { AdminMenuCategoryService } from '../../services/admin-menu-category/admin-menu-category.service';
import { AdminFoodOrderService } from '../../services/admin-food-order/admin-food-order.service';
import { CreateStaffService } from '../../services/add-staff/create-staff.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  public totalMenus;
  public totalUsers;
  public totalOrders;
  public totalMenuCategories;
  constructor(
    private menuService: AdminMenuService,
    private menuCatService: AdminMenuCategoryService,
    private orderService: AdminFoodOrderService,
    private registerService: CreateStaffService,
    ) { }

  ngOnInit() {
    this.getMenuCount();
    this.getMenuCategoryCount();
    this.getOrdersCount();
    this.getUsersCount();
  }

  //for menu count
  getMenuCount(){
    this.menuService.getMenuCount().subscribe(data =>
      this.totalMenus = data);
  }

   //for menu Category count
   getMenuCategoryCount(){
    this.menuCatService.getMenuCategoryCount().subscribe(data =>
      this.totalMenuCategories = data);
  }


   //for menu count
   getOrdersCount(){
    this.orderService.getOrderCount().subscribe(data =>
      this.totalOrders = data);
  }

   // get user list
   getUsersCount() {
    this.registerService
      .getUserDetails()
      .subscribe((data: any) => {
        this.totalUsers = data.length;
        console.log(this.totalUsers);
      });

  }
}
