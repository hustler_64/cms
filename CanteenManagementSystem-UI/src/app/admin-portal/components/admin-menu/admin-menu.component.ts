import { Component, OnInit, ViewChild } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  NgForm
} from "@angular/forms";
import { MatTableDataSource, MatPaginator, MatSort } from "@angular/material";
import { AdminMenuService } from "../../services/admin-menu/admin-menu.service";
import { UserModel, IUserModels } from "../../models/user-model";
import { ToastrService } from "ngx-toastr";
import { AdminMenuCategoryService } from "../../services/admin-menu-category/admin-menu-category.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-admin-menu",
  templateUrl: "./admin-menu.component.html",
  styleUrls: ["./admin-menu.component.scss"]
})
export class AdminMenuComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private menuService: AdminMenuService,
    private toastr: ToastrService,
    private menuCategoryService: AdminMenuCategoryService
  ) {}
  /////////////////// for tables/////////////////
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = [
    "id",
    "menuCatName",
    "menu",
    "price",
    "remarks",
    "action"
  ];

  menuForm: FormGroup;
  public menus: any;
  public menuCategories: any;

  ngOnInit() {
    //call function
    this.getMenu();
    this.getMenuCat();
    //initialize form
    this.menuForm = this.formBuilder.group({
      MenuName: new FormControl(null, Validators.required),
      Price: new FormControl(null, [Validators.required]),
      Remarks: new FormControl(null),
      id: new FormControl(null),
      CatId: new FormControl(null, [Validators.required]),
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  //reset form after submit
  resetForm(form?: NgForm) {
    if (form != null) form.reset();
    this.menuForm = this.formBuilder.group({
      MenuName: [""],
      Price: [""],
      Remarks: [""],
      CatId: [""],
      id: ['']
    });
  }

  //mennu details
  getMenu() {
    this.menuService.getMenuName().subscribe((data: any) => {
      this.menus = data;
      //to populate on table
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
    });
  }

  //get menu deails
  getMenuCat() {
    this.menuCategoryService.getMenuCategoryName().subscribe((data: any) => {
      this.menuCategories = data;
    });
  }

  OnSubmit(saveMenu) {
    if (saveMenu.id == null) {
      this.menuService.saveMenu(saveMenu).subscribe((data: any) => {
        if (data.result == true) {
          this.resetForm();
          this.getMenu();
          this.toastr.success(
            "Menu successfully added!",
            "Menu"
          );
        } else {
          this.toastr.error("Unable to add menu!", "Menu");
        }
      });
    } else {
      this.menuService.updateMenu(saveMenu).subscribe((data: any) => {
        if (data.result == true) {
          this.resetForm();
          this.getMenu();
          this.toastr.success(
            "Menu successfully updated!",
            "Menu"
          );
        } else {
          this.toastr.error("Menu not updated", "Menu");
        }
      });
    }
  }

  onDelete(id: string) {
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#72BF44',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes, Delete!'
    }).then((result) => {
      if (result.value) {
        this.menuService.deleteMenu(id)
        .subscribe((data: any) => {
          if (data.result == true) {
            this.toastr.success('Menu successfully deleted!', 'Menu');
            this.getMenuCat();
          }
          else {
            this.toastr.error('Menu not deleted', 'Menu');
          }
        });
    }
    });
  }

 //to edit the value from the table
 showForEdit(menuDetails: any) {
   console.log(menuDetails);
  this.menuForm.setValue({
    MenuName: menuDetails.menu,
    Price: menuDetails.price,
    Remarks: menuDetails.remarks,
    CatId: menuDetails.menuCatId,
    id: menuDetails.id
  });
}
}
