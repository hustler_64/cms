import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AdminMenuCategoryService } from '../../services/admin-menu-category/admin-menu-category.service';
import { ToastrService } from 'ngx-toastr';
import { MenuCategoryModel } from '../../models/menu-category-model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-menu-category',
  templateUrl: './admin-menu-category.component.html',
  styleUrls: ['./admin-menu-category.component.scss']
})
export class AdminMenuCategoryComponent implements OnInit {

  menuForm: FormGroup;
  public menuCategories: any;

  constructor(
    private formBuilder: FormBuilder,
    private menuCategoryService: AdminMenuCategoryService,
    private toastr: ToastrService,
    ) {

  }
  /////////////////// for tables/////////////////
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['id', 'menuCategoriesName', 'remarks', 'action' ];

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  ///////////////////


  ngOnInit() {
    this.menuForm = this.formBuilder.group({
      MenuCategoryName:  new FormControl(null, Validators.required),
      Remarks: new FormControl(null),
      id: new FormControl(null)
    });

    this.getMenuCat();

  }

    //reset form after submit
    resetForm(form?: NgForm) {
      if (form != null)
        form.reset();
      this.menuForm = this.formBuilder.group({
        MenuCategoryName: [''],
        Remarks: [''],
        id: ['']
      });
    }


  //get menu deails
  getMenuCat(){
    this.menuCategoryService.getMenuCategoryName().subscribe((data: any) => {
      this.menuCategories = data
      console.log(data)
      //to populate on table
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
    });
  }


  //to edit the value from the table
  showForEdit(menuCategory: any) {
    this.menuForm.setValue({
      MenuCategoryName: menuCategory.menuCategoriesName,
      Remarks: menuCategory.remarks,
      id: menuCategory.id
    });
  }

  OnSubmit(saveMenuCat) {
    if (saveMenuCat.id == null) {
      this.menuCategoryService.saveMenuCategories(saveMenuCat)
        .subscribe(
          (data: any) => {
            if (data.id == null) {
              this.resetForm();
              this.getMenuCat();
              this.toastr.success('Menu Category successfully added!', 'Menu Category');
            } else {
              this.toastr.error('Unable to add menu category!', 'Menu Category');
            }
          });
    }
    else {
      this.menuCategoryService.updateCategory(saveMenuCat)
        .subscribe((data: any) => {
          if (data.result == true) {
            this.resetForm();
            this.getMenuCat();
            this.toastr.success('Menu Category successfully updated!', 'Menu Category');
          }
          else {
            this.toastr.error('Menu Category  not updated', 'Menu Category');
          }
        });
    }
  }


  onDelete(id: string) {
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#72BF44',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes, Delete!'
    }).then((result) => {
      if (result.value) {
        this.menuCategoryService.deleteCategory(id)
        .subscribe((data: any) => {
          if (data.result == true) {
            this.getMenuCat();
            this.toastr.success('Menu Category successfully deleted!', 'Menu Category');
          }
          else {
            this.toastr.error('Menu Category  not deleted', 'Menu Category');
          }
        });
    }
    });
  }

}

