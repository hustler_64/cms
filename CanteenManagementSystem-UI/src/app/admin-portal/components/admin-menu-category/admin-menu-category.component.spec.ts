import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMenuCategoryComponent } from './admin-menu-category.component';

describe('AdminMenuCategoryComponent', () => {
  let component: AdminMenuCategoryComponent;
  let fixture: ComponentFixture<AdminMenuCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminMenuCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMenuCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
