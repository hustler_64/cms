import { Component, OnInit } from "@angular/core";
import { AdminLoginService } from "../../services/admin-login/admin-login.service";
import { Router, ActivatedRoute } from "@angular/router";
import { UserModel } from "../../models/user-model";
import {
  FormGroup,
  Validators,
  FormBuilder,
  NgForm,
  FormControl
} from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";

@Component({
  selector: "app-admin-login",
  templateUrl: "./admin-login.component.html",
  styleUrls: ["./admin-login.component.scss"]
})
export class AdminLoginComponent implements OnInit {
  errormessage = "";
  user: UserModel;
  returnUrl = "";
  myData;
  loginForm: FormGroup;

  constructor(
    private adminLoginService: AdminLoginService,
    public router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {}



  ngOnInit() {
    // login form group
    this.loginForm = this.formBuilder.group({
      userName:  new FormControl(null, Validators.required),
      password: new FormControl(null, [Validators.required, Validators.minLength(7)])
    });

    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
    this.checkLoggedIn();
  }

  get f() { return this.loginForm.controls; }

  checkLoggedIn() {
    this.adminLoginService.checkLoggedIn();
  }


    //reset form after submit
    resetForm(form?: NgForm) {
      if (form != null)
        form.reset();
      this.loginForm = this.formBuilder.group({
        userName: [''],
        password: ['']
      });
    }

  /** when login Button is clicked, this function is used for authencation */
  onLogin(loginValue) {
    // console.log(loginValue);
    this.adminLoginService.login(loginValue).subscribe(
      res => {
        localStorage.setItem("auth_token", res.token);
        localStorage.setItem("auth_role", res.role.toString());
        localStorage.setItem("userName", res.userName);
        localStorage.setItem("userId", res.userId);

        if (localStorage.auth_token === res.token) {
          this.adminLoginService.authenticate(true);
          this.adminLoginService.checkLoggedIn();
        }

      },
      (error) => (Swal.fire({
        position: "top-end",
        type: "error",
        title: "Login Failed!",
        showConfirmButton: false,
        timer: 2000
      })));
  }


}
