import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminFoodOrderComponent } from './admin-food-order.component';

describe('AdminFoodOrderComponent', () => {
  let component: AdminFoodOrderComponent;
  let fixture: ComponentFixture<AdminFoodOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminFoodOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminFoodOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
