import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { MatTableDataSource, MatPaginator, MatSort } from "@angular/material";
import { AdminFoodOrderService } from "../../services/admin-food-order/admin-food-order.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as jspdf from "jspdf";
import { DatePipe } from '@angular/common';
import html2canvas from "html2canvas";

@Component({
  selector: "app-admin-food-order",
  templateUrl: "./admin-food-order.component.html",
  styleUrls: ["./admin-food-order.component.scss"],
  providers: [DatePipe]
})
export class AdminFoodOrderComponent implements OnInit {
  public foodOrders: any;
  public perDaySales: any;
  public userRole: any;
  currentDates;

  disabledAgreement: boolean = false;
  changeCheck(event){
    this.disabledAgreement = event.checked;
  }

  constructor(
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private foodOrderService: AdminFoodOrderService,
    private datePipe: DatePipe
  ) {
  }
  ngOnInit() {
    this.getOrderDetails();
    this.getPerDaySales();
    this.currentDates = this.datePipe.transform(new Date(), 'MMMM d, y');

    this.userRole = localStorage.getItem("auth_role");
    console.log(this.userRole);
  }

  //get per day sales
  getOrderDetails() {
    this.foodOrderService.getOrderDetails().subscribe((data: any) => {
      this.foodOrders = data;
      //to populate on table
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
    });
  }

  //get per day sales
  getPerDaySales() {
    this.foodOrderService.getPerDaySales().subscribe((data: any) => {
      this.perDaySales = data;
        //to populate on table
        this.perDaySale = new MatTableDataSource(data);
        this.perDaySale.paginator = this.paginator;
    });
  }

  /////////////////// for tables/////////////////
  dataSource;
  perDaySale;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = [
    "id",
    "menuCatrogoryName",
    "menuName",
    "userName",
    "totalPrice",
    "orderRemarks",
    "action"
  ];

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.perDaySale.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    if (this.perDaySale.paginator) {
      this.perDaySale.paginator.firstPage();
    }
  }
  ///////////////////

  //open invoice modal
  userName: '';
  currentDate: '';
  catName: '';
  foodName: '';
  price: '';
  totalPrice: ''
  tableNumber: '';
  open(data, invoice) {
      console.log(data);
    this.modalService.open(invoice);
    this.userName = data.userName;
    this.currentDate = this.currentDates;
    this.catName = data.menuCatrogoryName;
    this.foodName = data.menuName;
    this.price = data.price;
    this.totalPrice = data.totalPrice;
    this.tableNumber = data.tableNumber;
  }

  show(invoice) {
    console.log(this.perDaySales);
  this.modalService.open(invoice);

}

  public captureScreen() {
    var data = document.getElementById("contentToConvert");
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL("image/png");
      let pdf = new jspdf("p", "mm", "a4"); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
      pdf.save("invoice.pdf"); // Generated PDF
    });
  }
}
