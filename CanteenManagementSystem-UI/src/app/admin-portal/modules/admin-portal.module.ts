import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AdminLoginComponent } from '../components/admin-login/admin-login.component';
import { AdminDashboardComponent } from '../components/admin-dashboard/admin-dashboard.component';
import { HttpClientModule } from '@angular/common/http';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';

import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminMenuCategoryComponent } from '../components/admin-menu-category/admin-menu-category.component';
import { AdminMenuComponent } from '../components/admin-menu/admin-menu.component';
import { AdminFoodOrderComponent } from '../components/admin-food-order/admin-food-order.component';
import { DemoMaterialModule } from '../../../app/material.module';

import { ForgotPasswordComponent } from '../../shared/components/forgot-password/forgot-password.component';
import { CreateStaffComponent } from '../components/create-staff/create-staff.component';
// ROUTING FOR THIS MODULES
const routes: Routes = [
  {
    path: '',
    children: [
        {path: 'login', component: AdminLoginComponent},
        {path: 'dashboard', component: AdminDashboardComponent, canActivate: [AuthGuard]},
        {path: 'menuCategory', component: AdminMenuCategoryComponent, canActivate: [AuthGuard]},
        {path: 'menu', component: AdminMenuComponent, canActivate: [AuthGuard]},
        {path: 'foorOrder', component: AdminFoodOrderComponent, canActivate: [AuthGuard]},
        {path: 'createStaff', component: CreateStaffComponent, canActivate: [AuthGuard]},
        {path: 'forgetPassword', component: ForgotPasswordComponent},
    ]
  },
]
@NgModule({
  imports: [
    CommonModule,
    DemoMaterialModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    RouterModule.forChild(routes),
    MDBBootstrapModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    CountdownTimerModule.forRoot(),
  ],
  declarations: [ForgotPasswordComponent,AdminLoginComponent, AdminDashboardComponent, AdminMenuCategoryComponent, AdminMenuComponent, AdminFoodOrderComponent, CreateStaffComponent]
})
export class AdminPortalModule { }
