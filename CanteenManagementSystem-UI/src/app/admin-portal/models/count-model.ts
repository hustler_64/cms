export interface ITotal{
  menuCount: number,
  userCount: number
  menuCategoryCount: number,
  orderCount: number
}
