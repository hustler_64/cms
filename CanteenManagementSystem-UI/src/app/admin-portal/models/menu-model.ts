export class MenuModel {
  menu: string;
  price: number;
  remarks: string;
  menuCatName: string;
  menuCatId: number;
  menuCatRemarks: string;
}

export interface IMenus{
  menu: string;
  price: number;
  remarks: string;
  menuCatName: string;
  menuCatId: number;
  menuCatRemarks: string;
}
