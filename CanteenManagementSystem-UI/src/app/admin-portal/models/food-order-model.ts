export class OrderModel {
  id: number;
  customerName: string;
  totalPrice: number;
  price: number;
  orderRemarks: string;
  menuCatrogoryName: string;
  menuName: string;
  tableNumber: number;
}

export interface IOrders{
  id: number;
  customerName: string;
  totalPrice: number;
  price: number;
  orderRemarks: string;
  menuCatrogoryName: string;
  menuName: string;
  tableNumber: number;
}

