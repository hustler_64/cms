export class UserModel {
    email: string;
    firstName: string;
    userId: string;
    lastName: string;
    lockoutEnabled?: boolean;
    lockoutEnd?: any;
    password: string;
    phoneNumber?: any;
    role: number;
    status?: number;
    userName: string;
}


export class IUserModels {
  email: string;
  firstName: string;
  userId: string;
  lastName: string;
  lockoutEnabled?: boolean;
  lockoutEnd?: any;
  password: string;
  phoneNumber?: any;
  role: number;
  status?: number;
  userName: string;
}

export interface IUserRegister{
  email: string;
  firstName: string;
  id: string;
  lastName: string;
  password: string;
  userName: string;
}
