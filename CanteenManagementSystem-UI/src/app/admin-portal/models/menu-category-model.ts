export class MenuCategoryModel {
  Remarks: string;
  MenuCategoriesName: string;
  CreatedById: string;
}

export interface IMenuCategories{
  remarks: string;
  menuCategoriesName: string;
  id: number;
}
