import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { AdminLoginService } from 'src/app/admin-portal/services/admin-login/admin-login.service';
import { Router, NavigationEnd } from '@angular/router';
import Swal from 'sweetalert2';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit, OnDestroy {
  public userRole: any;

  constructor(private adminLoginService: AdminLoginService, private router: Router,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {

      this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.checkToken();
      };
    });
  }

  show: boolean = false;

  ngOnInit() {
    this.checkToken();

    this.userRole = localStorage.getItem("auth_role");
    console.log(this.userRole);
  }

  //check token to change the header
  checkToken(){
    var token = localStorage.getItem('auth_token');
    if(token != null){
      this.show = true;
    }
    else{
      this.show = false;
    }
  }

  //logout from the application
  logout(){
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#72BF44',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes, Logout!'
    }).then((result) => {
      if (result.value) {
        this.adminLoginService.logoutUser();

      }
    });
  }


  ////////////////////////////////////////////////////
  mobileQuery: MediaQueryList;

  fillerNav = Array.from({length: 50}, (_, i) => `Food Menu ${i + 1}`);

  fillerContent = Array.from({length: 50}, () =>
      `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
       labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
       laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
       voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
       cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);

  private _mobileQueryListener: () => void;



  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  sidenavWidth = 4;
  ngStyle: string;


  increase() {
    this.sidenavWidth = 15;
  }
  decrease() {
    this.sidenavWidth = 4;
  }
}
