import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class HomeService {
  constructor(private _http: HttpClient) {}

  // http interceptor
  httpOptions() {
    var token = localStorage.getItem("auth_token");
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      })
    };
    return httpOptions;
  }
  //save ContactUs
  saveContactUs(contactUs: any) {
    const contactDetails: any = {
      name: contactUs.name,
      message : contactUs.message,
      subject : contactUs.subject,
      email : contactUs.email
    };
    return this._http.post(
      `${environment.apiUrl}Account/SaveMessage`,
      contactDetails,
      this.httpOptions()
    );
  }

  //save order
  SaveOrder(order: any) {
    const orderDetails: any = {
      menuId: order.MenuId.id,
      customerName: order.customerName,
      tableNumber: order.tableNumber,
      remarks: order.MenuId.remarks,
      totalPrice: order.MenuId.price
    };
    return this._http.post(
      `${environment.apiUrl}Orders/saveOrders`,
      orderDetails,
      this.httpOptions()
    );
  }
}
