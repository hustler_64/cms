import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from '../components/home/home.component';
import { OrderComponent } from '../components/order/order.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';

import {DemoMaterialModule} from './material-module';
import { ContactUsComponent } from '../components/contact-us/contact-us.component';
// import { UserLoginComponent } from '../components/user-login/user-login.component';
import { AuthGuard } from 'src/app/shared/guards/auth.guard';
// ROUTING FOR THIS MODULES
const routes: Routes = [
  {
    path: '',
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'order', component: OrderComponent},
      {path: 'contactUs', component: ContactUsComponent},
      // {path: 'login', component: UserLoginComponent},

    ]
  },


];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MDBBootstrapModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    DemoMaterialModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,

  ],
  declarations: [HomeComponent,  OrderComponent, ContactUsComponent]
})
export class MarketingSiteModule { }
