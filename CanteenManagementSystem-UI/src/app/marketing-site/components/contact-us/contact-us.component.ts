import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { HomeService } from '../../services/home/home.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  contactForm: FormGroup;

  constructor(
  private formBuilder: FormBuilder,
  private homeService: HomeService,
  private toastr: ToastrService

  ) { }

ngOnInit() {
  this.contactForm = this.formBuilder.group({
    userName:  new FormControl(null, Validators.required),
    email: ['', [Validators.required, Validators.email]],
    subject: new FormControl(null, [Validators.required]),
    message: new FormControl(null, [Validators.required])
  });
}

 //reset form after submit
 resetForm(form?: NgForm) {
  if (form != null)
    form.reset();
  this.contactForm = this.formBuilder.group({
    userName: [''],
    email: [''],
    subject: [''],
    message: ['']
  });
}

onSubmit(contactValue){
  this.homeService.saveContactUs(contactValue)
  .subscribe(
    (data: any) => {
      if (data.id == null) {
        this.resetForm();
        this.toastr.success('Your Message is saved!', 'Contact Us');
      } else {
        this.toastr.error('Unable to save your message!', 'Contact Us');
      }
    });
}
}
