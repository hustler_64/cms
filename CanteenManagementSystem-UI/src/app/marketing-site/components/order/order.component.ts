import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { AdminMenuService } from 'src/app/admin-portal/services/admin-menu/admin-menu.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as jspdf from "jspdf";
import { DatePipe } from '@angular/common';
import html2canvas from "html2canvas";
import { HomeService } from '../../services/home/home.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  providers: [DatePipe]

})
export class OrderComponent implements OnInit {
  selectedValue: string;
  orderForm: FormGroup;
  public menus: any;
  currentDates;

  constructor
  (
    private formBuilder: FormBuilder,
    private menuService: AdminMenuService,
    private modalService: NgbModal,
    private datePipe: DatePipe,
    private homeService: HomeService,
    private toastr: ToastrService,

  ) { }


  ngOnInit() {
    // login form group
    this.orderForm = this.formBuilder.group({
      customerName:  new FormControl(null, Validators.required),
      tableNumber: new FormControl(null, [Validators.required]),
      MenuId: new FormControl(null, [Validators.required]),
    });
    this.currentDates = this.datePipe.transform(new Date(), 'MMMM d, y');

    this.getMenu();
  }

   //mennu details
   getMenu() {
    this.menuService.getMenuName().subscribe((data: any) => {
      this.menus = data;
    });
  }


   public captureScreen() {
    var data = document.getElementById("contentToConvert");
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL("image/png");
      let pdf = new jspdf("p", "mm", "a4"); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, "PNG", 0, position, imgWidth, imgHeight);
      pdf.save("invoice.pdf"); // Generated PDF
    });
  }

    //reset form after submit
    resetForm(form?: NgForm) {
      if (form != null)
        form.reset();
      this.orderForm = this.formBuilder.group({
        customerName: [''],
        tableNumber: [''],
        MenuId: ['']
      });
    }


  OnSubmit(addOrder, invoice){
    console.log(addOrder);
    this.homeService.SaveOrder(addOrder)
    .subscribe(
      (data: any) => {
        if (data.id == null) {
          this.resetForm();
          this.open(addOrder, invoice);
          this.toastr.success('Your order has been added!', 'Food Order');
        } else {
          this.toastr.error('Unable to save your orders!', 'Food Order');
        }
      });
  }

  //open invoice modal
  userName: '';
  currentDate: '';
  catName: '';
  foodName: '';
  quantity: '';
  price: '';
  totalPrice: ''
  tableNumber: '';
  open(data, invoice) {
      console.log(data);
    this.modalService.open(invoice);
    this.userName = data.customerName;
    this.currentDate = this.currentDates;
    this.tableNumber = data.tableNumber;
    this.foodName = data.MenuId.menu;
    this.price = data.MenuId.price;
    this.totalPrice = data.MenuId.price;
  }


}
