import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { AdminMenuCategoryService } from 'src/app/admin-portal/services/admin-menu-category/admin-menu-category.service';
import { AdminMenuService } from 'src/app/admin-portal/services/admin-menu/admin-menu.service';
import { HomeService } from '../../services/home/home.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  contactForm: FormGroup;
  public menus: any;
  public menuCategories: any;
    constructor(
    private formBuilder: FormBuilder,
    private menuCategoryService: AdminMenuCategoryService,
    private menuService: AdminMenuService,
    private homeService: HomeService,
    private toastr: ToastrService

    ) { }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      userName:  new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required]),
      subject: new FormControl(null, [Validators.required]),
      message: new FormControl(null, [Validators.required])
    });

    this.getMenuCat();


  }

   //reset form after submit
 resetForm(form?: NgForm) {
  if (form != null)
    form.reset();
  this.contactForm = this.formBuilder.group({
    userName: [''],
    email: [''],
    subject: [''],
    message: ['']
  });
}

  onSubmit(contactValue){
    this.homeService.saveContactUs(contactValue)
    .subscribe(
      (data: any) => {
        if (data.id == null) {
          this.resetForm();
          this.toastr.success('Your Message is saved!', 'Contact Us');
        } else {
          this.toastr.error('Unable to save your message!', 'Contact Us');
        }
      });
  }
  //get menu deails
  getMenuCat(){
    this.menuCategoryService.getMenuCategoryName().subscribe((data: any) => {
      this.menuCategories = data;
    });
  }

   //mennu details
   getMenu(id: string) {
    this.menuService.getMenuDetailsByCategoryId(id).subscribe(
      data =>
      {
        this.menus = data;
        console.log(this.menus)
      });
  }
}
